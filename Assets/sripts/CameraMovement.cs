﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform Player;

    public Vector3 Offset;

    private void FixedUpdate()
    {
        Vector3 TargetPosition = Player.position + Offset;

        transform.position = TargetPosition;

    }

}
