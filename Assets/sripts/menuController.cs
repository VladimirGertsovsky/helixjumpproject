﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menuController : MonoBehaviour
{
    public GameObject ButtonsMenu;
    public GameObject ButtonsExit;
    public GameObject ButtonsLevel;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowExitButtons()
    {
        ButtonsMenu.SetActive(false);
        ButtonsExit.SetActive(true);
        ButtonsLevel.SetActive(false);
    }

    public void BackInMenu()
    {
        ButtonsMenu.SetActive(true);
        ButtonsExit.SetActive(false);
        ButtonsLevel.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void NewGameLoadSceneGame()
    {
        Application.LoadLevel("Game");
    }

    public void levelbutton()
    {
        ButtonsExit.SetActive(false);
        ButtonsMenu.SetActive(false);
        ButtonsLevel.SetActive(true);

    }

}
