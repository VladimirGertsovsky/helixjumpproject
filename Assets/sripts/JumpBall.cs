﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class JumpBall : MonoBehaviour
{
    public float JumpSpeed;

    Rigidbody rb;

    public GameObject SplashParticle;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }


    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Platform")
        {

            GameObject Splash = Instantiate(SplashParticle, transform.position, transform.rotation);
            Splash.transform.parent = collision.gameObject.transform;
            rb.AddForce(Vector3.up * JumpSpeed * Time.deltaTime, ForceMode.Impulse);
        }

        if(collision.gameObject.tag == "Obstacle")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        if (collision.gameObject.tag == "Finish")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        }
    }
    

 
}
